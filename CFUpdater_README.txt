What is this
-------------

CFUpdate will update records in your Cloudflare-account. From here on, Cloudflare is shortened to CF. 
You are probably already aware that CF provides free DNS-services for your domain(s), this utility uses their API to update records on-demand to serve as a reliable dynamic DNS-provider. Problem is that all updaters for it suck donkeycock, so here is one that does not.

How to use it
-------------

Get your API-key by logging in to your CF-account and fetching it. This key-token goes into the .ini-file (tkn=), along with your email, domain(zone)name and the host you wish to update.
You can leave TYPE and TTL as is, or modify them if you know what you are doing. The CFUpdater.ini-EXAMPLE file is pretty selfexplaining. 

How to run it
-------------

Prerequisite: None.

Schedule/crontab CFUpdater(.exe) to run, done. Example, add this to crontab to run every 30 minutes and at systemboot:

@reboot /path/to/CFUpdater
*/30 * * * * /path/to/CFUpdater

Windows: Open "Task Scheduler", create a new task. point to CFUpdater.exe file, done. Set it to run as desired, preferably on reboot and randomly once every hour. 

Optional
--------

If you want to update multiple domains, make a copy of your ini-file and make the desired modifications, then run:

$ CFUpdater --config otherdomain.ini

If something goes wrong, run it manually and increase verbosity to DEBUG:

$ CFUpdater -vv

... and it should tell you in console / CFUpdate.log what is wrong. Fix it. If you are unable to, contact me and I'll fix it.

-Bjorn "GRYZOR" Rohl�n (gryzor@safehex.se) af Skid Row-fame
