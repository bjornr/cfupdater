import logging as l
import traceback
from argparse import ArgumentParser
from configparser import ConfigParser
from json import dumps
from json import loads
from logging.handlers import TimedRotatingFileHandler
from sys import exit

from requests import get
from requests import put

cf_version = "1.1c"

parser = ArgumentParser(description="Update Cloudflare DNS records dynamically.")
parser.add_argument("-v", "-vv", "--verbose", action="count", default=0,
                    help="Increase verbosity. The more, the merrier.")
parser.add_argument("-c", "--config", help="Specifies the configuration file to use, else use default CFUpdater.ini")
args = parser.parse_args()

config = ConfigParser()

if args.config:
    inifile = args.config
else:
    inifile = "CFUpdater.ini"

try:
    with open(inifile, 'r') as configfile:
        config.read_file(configfile)
except Exception as e:
    l.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    l.critical(e)
    l.critical("You need to use --config and specify your ini-file, if CFUpdater.ini is not in current directory.")
    traceback.print_exc()
    exit(-1)
if args.verbose >= 2:
    logLevel = "DEBUG"
    config['DEFAULT']['logToConsole'] = True
elif args.verbose == 1:
    logLevel = "INFO"
    config['DEFAULT']['logToConsole'] = True
else:
    logLevel = config['DEFAULT']['logLevel']

l.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logLevel, handlers=[])


def process_fhandler(fh):
    fh.setLevel(logLevel)
    f = l.Formatter('%(asctime)s %(levelname)s: %(message)s')
    fh.setFormatter(f)
    l.getLogger().addHandler(fh)

if "TRUE" in config['DEFAULT']['logToConsole'].upper():
    process_fhandler(l.StreamHandler())

if "TRUE" in config['DEFAULT']['logToFile'].upper():
    process_fhandler(TimedRotatingFileHandler(filename='CFUpdater.log', when='midnight', interval=1, backupCount=5))

l.getLogger("requests").setLevel(l.WARNING)


class URLOpener(object):
    def __init__(self, request):
        self.r = None
        self.request = request
        self.ip_data = None
        self.headers = None

    def open_url(self, data=None):
        try:
            if data is None:
                self.r = get(self.request, headers=self.headers)
            else:
                self.r = put(self.request, data=data, headers=self.headers)
        except Exception as ex:
            l.error(ex.args)
            exit(-1)

        try:
            self.ip_data = loads(self.r.text)
        except Exception as ex:
            l.critical("JSONDecodeException: %s -- aborting", ex)
            traceback.print_exc()
            exit(-1)

        if "success" in self.ip_data and not self.ip_data['success']:
            l.error("Something went wrong, this information might help:\n%s", self.ip_data)
            traceback.print_exc()
            exit(-1)

        elif "errors" in self.ip_data and len(self.ip_data['errors']) > 0:
            for i in range(len(self.ip_data['errors'])):
                l.critical("%s :: %s", self.ip_data['errors'][i]['code'], self.ip_data['errors'][i]['message'])
            l.critical("Aborting.")
            traceback.print_exc()
            exit(-1)

        elif "result" in self.ip_data and len(self.ip_data['result']) == 0:
            l.critical("Result-list empty.\nPossible clue to what went wrong:\n---------------------------------\n%s",
                       self.ip_data)
            traceback.print_exc()
            exit(-1)
        else:
            return self.ip_data

url = URLOpener("http://ip-api.com/json")
url.headers = {
    'X-Auth-Email': config['DEFAULT']['email'],
    'X-Auth-key': config['DEFAULT']['token'],
    'Content-Type': 'application/json',
    'per_page': '100'
}
result = url.open_url()

external_ip = result["query"]

l.debug(result)
l.debug("CFUpdater v{0} -- Location is {1}, {2}, using {3}.".format(cf_version, result["regionName"], result["country"],
                                                                    result["isp"]))
l.debug("External ip: " + external_ip)

url.request = "https://api.cloudflare.com/client/v4/zones?name=" + config['DEFAULT'][
    'zone'] + "&status=active&page=1&per_page=100&order=status&direction=desc&match=all"
result = url.open_url()

zone_id = result['result'][0]['id']

url.request = "https://api.cloudflare.com/client/v4/zones/" + zone_id + "/dns_records"
result = url.open_url()

record_name = config['DEFAULT']['host'] + "." + config['DEFAULT']['zone']
record_data = None

for x in range(result['result_info']['count']):
    if result['result'][x]['type'] == "A" or result['result'][x]['type'] == "CNAME":
        if result['result'][x]['name'] == record_name:
            l.debug("Found it! %s (%s) located with ip %s.", result['result'][x]['name'], result['result'][x]['id'],
                    result['result'][x]['content'])
            record_data = result['result'][x]
            break

if record_data is None:
    l.error("Unable to locate %s.", record_name)
    traceback.print_exc()
    exit(-1)

message = "Our external IP is " + external_ip + " and we will "

if record_data['content'] == external_ip:
    message += "not update, since it matches the IP on record."
    l.debug(message)
    traceback.print_exc()
    exit(0)

l.info(message + "update it, IP on record is %s.", record_data['content'])

url.request = "https://api.cloudflare.com/client/v4/zones/" + zone_id + "/dns_records/" + record_data['id']

record_data['content'] = external_ip
record_data = dumps(record_data)

result = url.open_url(record_data)

l.debug("Exiting normally.")
exit(0)
